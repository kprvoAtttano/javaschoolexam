package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Addition extends Pair {

    public Addition(Node left, Node right) {
        super(left, right);
    }

    @Override
     Object getValue() {
        Object oleft = left.getValue();
        Object oright = right.getValue();
        if ((oleft.getClass().getName() == "java.lang.Integer" && oright.getClass().getName() == "java.lang.Integer")){
            return (Integer)oleft + (Integer)oright;
        }
        if ((oleft.getClass().getName() == "java.lang.Integer" && oright.getClass().getName() == "java.lang.Double")){
            return (Integer)oleft + (Double)oright;
        }
        if ((oleft.getClass().getName() == "java.lang.Double" && oright.getClass().getName() == "java.lang.Integer")){
            return (Double)oleft + (Integer)oright;
        }
        if ((oleft.getClass().getName() == "java.lang.Double" && oright.getClass().getName() == "java.lang.Double")){
            return (Double)oleft + (Double)oright;
        }
        return null;
    }
    public static Node parse(String statement){
        int count = 0;
        int pos = 0;
        while (pos < statement.length()){
            if (statement.charAt(pos) == '(') count++;
            if (statement.charAt(pos) == ')') count--;
            if (statement.charAt(pos) == '+' && count == 0){
                return new Addition(Subtraction.parse(statement.substring(0, pos)), Addition.parse(statement.substring(pos + 1)));
            }
            pos++;
        }
        return Subtraction.parse(statement);
    }
}
