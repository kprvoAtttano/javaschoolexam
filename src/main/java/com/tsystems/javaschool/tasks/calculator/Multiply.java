package com.tsystems.javaschool.tasks.calculator;

public class Multiply extends Pair {
    public Multiply(Node left, Node right) {
        super(left, right);
    }

    public static Node parse(String statement) {
        int count = 0;
        int pos = 0;
        while (pos < statement.length()){
            if (statement.charAt(pos) == '(') count++;
            if (statement.charAt(pos) == ')') count--;
            if (statement.charAt(pos) == '*' && count == 0){
                return new Multiply(Division.parse(statement.substring(0, pos)), Multiply.parse(statement.substring(pos + 1)));
            }
            pos++;
        }
        return Division.parse(statement);
    }

    @Override
    Object getValue() {
        Object oleft = left.getValue();
        Object oright = right.getValue();
        if ((oleft.getClass().getName() == "java.lang.Integer" && oright.getClass().getName() == "java.lang.Integer")){
            return (Integer)oleft * (Integer)oright;
        }
        if ((oleft.getClass().getName() == "java.lang.Integer" && oright.getClass().getName() == "java.lang.Double")){
            return (Integer)oleft * (Double)oright;
        }
        if ((oleft.getClass().getName() == "java.lang.Double" && oright.getClass().getName() == "java.lang.Integer")){
            return (Double)oleft * (Integer)oright;
        }
        if ((oleft.getClass().getName() == "java.lang.Double" && oright.getClass().getName() == "java.lang.Double")){
            return (Double)oleft * (Double)oright;
        }
        return null;
    }
}
