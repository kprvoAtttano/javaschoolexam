package com.tsystems.javaschool.tasks.calculator;

public class Int extends Single {
    protected Integer value;

    public Int(Integer value){
        this.value = value;
    }

    Integer getValue(){
        return this.value;
    }

    public static Node parse(String statement){
        return new Int(Integer.parseInt(statement));
    }
}
