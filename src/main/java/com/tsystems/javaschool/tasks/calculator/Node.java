package com.tsystems.javaschool.tasks.calculator;

public abstract class Node {
    abstract Object getValue();

    static Node parse(String statement) {
        return Addition.parse(statement);
    }
}
