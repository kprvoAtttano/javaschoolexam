package com.tsystems.javaschool.tasks.calculator;

public class Division extends Pair {

    public Division(Node left, Node right) {
        super(left, right);
    }

    public static Node parse(String statement) {
        int count = 0;
        int pos = 0;
        while (pos < statement.length()){
            if (statement.charAt(pos) == '(') count++;
            if (statement.charAt(pos) == ')') count--;
            if (statement.charAt(pos) == '/' && count == 0){
                return new Division(Single.parse(statement.substring(0, pos)), Division.parse(statement.substring(pos + 1)));
            }
            pos++;
        }
        return Single.parse(statement);
    }

    @Override
    Object getValue() {
        Object oleft = left.getValue();
        Object oright = right.getValue();
        if ((oleft.getClass().getName() == "java.lang.Integer" && oright.getClass().getName() == "java.lang.Integer")){
            if((Integer)oleft % (Integer)oright == 0)
                return (Integer)oleft / (Integer)oright;
            return (Double)((double)((Integer)oleft).intValue()) / (Integer)oright;
        }
        if ((oleft.getClass().getName() == "java.lang.Integer" && oright.getClass().getName() == "java.lang.Double")){
            return (Integer)oleft / (Double)oright;
        }
        if ((oleft.getClass().getName() == "java.lang.Double" && oright.getClass().getName() == "java.lang.Integer")){
            return (Double)oleft / (Integer)oright;
        }
        if ((oleft.getClass().getName() == "java.lang.Double" && oright.getClass().getName() == "java.lang.Double")){
            return (Double)oleft / (Double)oright;
        }
        return null;
    }
}
