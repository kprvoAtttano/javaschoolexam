package com.tsystems.javaschool.tasks.calculator;

public class Parentheses extends Single {

    public static Node parse(String statement){
        int start = statement.indexOf('(');
        int finish = statement.lastIndexOf(')');
        return Addition.parse(statement.substring(start + 1, finish));
    }

}
