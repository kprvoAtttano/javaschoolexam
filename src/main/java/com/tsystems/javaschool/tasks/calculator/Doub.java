package com.tsystems.javaschool.tasks.calculator;

public class Doub extends Single {

    protected Double value;

    public Doub(Double value){
        this.value = value;
    }

    Double getValue(){
        return this.value;
    }

    public static Node parse(String statement){
        return new Doub(Double.parseDouble(statement));
    }
}
