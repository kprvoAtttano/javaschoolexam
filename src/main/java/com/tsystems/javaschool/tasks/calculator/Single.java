package com.tsystems.javaschool.tasks.calculator;

public abstract class Single extends Node {
    private Object value;

    public static Node parse(String statement) {
        //return null;
        if (statement.contains("(")) return Parentheses.parse(statement);
        else if (statement.contains(".")) return Doub.parse(statement);
        else return Int.parse(statement);
    }

    @Override
    Object getValue() {
        return value;
    }
}
