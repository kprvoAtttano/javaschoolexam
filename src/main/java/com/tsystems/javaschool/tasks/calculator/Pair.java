package com.tsystems.javaschool.tasks.calculator;

public abstract class Pair extends Node {
    protected Node left;
    protected Node right;

    public Pair (Node left, Node right){
        this.left = left;
        this.right = right;
    }
}
