package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    private int number;

    public int getNumber(){
        return number;
    }
    public CannotBuildPyramidException(String message, int num){
        super(message);
        number = num;
    }
}
