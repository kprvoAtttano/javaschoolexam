package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
            if (inputNumbers.contains(null))
                throw new CannotBuildPyramidException("Pyramid contains null value", 1);

            double doubhPyramid = (((1 + Math.sqrt(1 + 8 * inputNumbers.size())))/2) - 1;

            if (!isInt(doubhPyramid))
                throw new CannotBuildPyramidException("Pyramid cannot be build because of illegal " +
                        "number of elements in pyramid", 1);

            int hPyramid = (int)(doubhPyramid);
            Collections.sort(inputNumbers);
            int k = 0;

            int[][] pyramidArr = new int [hPyramid][2 * hPyramid - 1];

            for (int i = 0; i < pyramidArr.length; i++){
                int pos = 1;
                for (int zero1 = 1; zero1 < pyramidArr.length - i; zero1++){
                    pyramidArr[i][zero1 - 1] = 0;
                }

                int sum = 2 * (i + 1) - 1;
                int sumj = pyramidArr.length - i - 1;
                for (int j = sumj; j < sum + sumj; j++){
                    if (pos % 2 == 0)
                        pyramidArr[i][j] = 0;
                    else{
                        pyramidArr[i][j] = inputNumbers.get(k);
                        k++;
                    }
                    pos++;
                }

                for(int zero2 = pyramidArr.length + i; zero2 < pyramidArr[i].length; zero2++){
                    pyramidArr[i][zero2] = 0;
                }
            }
            return pyramidArr;
        }
    private boolean isInt(double number){
        return number % 1 == 0;
    }
}
