package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
            if (x == null || y == null){
                throw new IllegalArgumentException();
            }
            int key = 0;
            int k = 0;
            for (int i = 0; i < x.size(); i++){
                for (int j = k; j < y.size(); j++){
                    Object seqX = x.get(i);
                    Object seqY = y.get(j);
                    if (seqX.equals(seqY)){
                        key++;
                        k = j + 1;
                        break;
                    }
                }
            }
            if (key == x.size())
                return true;
        return false;
    }
}
